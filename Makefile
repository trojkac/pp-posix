CCFLAGS=-pedantic -Wall 
ifeq (run,$(firstword $(MAKECMDGOALS)))
  # use the rest as arguments for "run"
  RUN_ARGS := $(wordlist 2,$(words $(MAKECMDGOALS)),$(MAKECMDGOALS))
  # ...and turn them into do-nothing targets
  $(eval $(RUN_ARGS):;@:)
endif
main: main.o
	gcc -o main main.o -lpthread
main.o: main.c
	gcc ${CCFLAGS} -o main.o -c main.c

.PHONY: compile run

compile:
	gcc ${CCFLAGS} -o main main.c -lpthread
run: 
	@./main ${RUN_ARGS}

