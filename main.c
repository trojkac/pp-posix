#include<unistd.h>
#include<stdlib.h>
#include<stdio.h>
#include<string.h>
#include<pthread.h>
#include<sys/time.h>
#include<errno.h>
#define ERR(string)(__extension__({perror((string));\
					exit(EXIT_FAILURE);}))
#define TEMP_FAILURE_RETRY(expression)(__extension__ ({ long int __result; \
do __result = (long int) (expression);\
while (__result == -1L && errno == EINTR);\
__result; }))

#define MINRANGE 100
#define MAXRANGE 500
#define MINARGS 2
#define MAX_SHOTS 10
#define RYCERZ 0x12345678
#define KROL 0x87654321

typedef int biesiadnik_t ;

int n;
pthread_t *tids;


/* zasoby przynoszone przez kelnera*/
int dzban = 0;
int ogorki = 0; 
pthread_mutex_t dobra_lock = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t dobra_cond = PTHREAD_COND_INITIALIZER;



/*oczekiwanie rycerzy na naczynia(kielich i talerzyk)*/
pthread_mutex_t dost_dla_rycerza = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t *dostepnosc_dla_rycerza_cond; 
int* czy_czeka;
int* naczynia;

/*ktory rycerz opowiada*/
pthread_mutex_t opowiadanie_rycerzy_mutex = PTHREAD_MUTEX_INITIALIZER;
pthread_cond_t *opowiadanie_rycerzy_cond;
int* opowiadanie_rycerzy; 



void indeksy(int kto,int* kielich,int* talerz);
int czy_ma_naczynia(int kto);
int hard_sleep(int seconds,int nanoseconds);
void czekaj_na_dogodny_moment(int kto, biesiadnik_t typ,int na_opowiadanie);


void zabierz_kielich_talerzyk(int kto);
int zabierz_wino_ogorka(int kto);
void oddaj_kielich_talerzyk(int kto);


void opowiadanie(int kto, biesiadnik_t typ);
void spozywanie(int kto, biesiadnik_t typ);

void* king_thread(void* arg);
void* knight_thread(void* arg);
void* kelner_thread(void* arg);

void usage(char* name);

int main(int argc,char** argv)
{
	pthread_attr_t thread;
	pthread_t kelner;
	int *ids;
	int i;
	pthread_condattr_t attrcond;
	
	if(argc != MINARGS)
		usage(argv[0]);
	
	n = atoi(argv[1]);
	
	if( n < 5 || n % 2 == 0 )
		usage(argv[0]);
	srand((unsigned int)time(NULL));

	dzban = n/2;
	ogorki = n/3;

	dostepnosc_dla_rycerza_cond = (pthread_cond_t*) calloc(n+1,sizeof(pthread_cond_t));
	naczynia =   (int*) calloc((n+1),sizeof(int));
	czy_czeka =  (int*) calloc((n+1),sizeof(int));
	tids =       (pthread_t*)calloc((n+1),sizeof(pthread_t));
	ids =        (int*)calloc((n+1),sizeof(int));
	opowiadanie_rycerzy = (int*)calloc((n+1),sizeof(int));
	opowiadanie_rycerzy_cond = (pthread_cond_t*)calloc((n+1),sizeof(pthread_cond_t));
	pthread_condattr_init(&attrcond);
	for(i=0;i<=n;i++)
	{
		czy_czeka[i] = 0;
		naczynia[i] = 1;
		ids[i] = i;
		opowiadanie_rycerzy[i] = 0;
		pthread_cond_init(&dostepnosc_dla_rycerza_cond[i],&attrcond);
		pthread_cond_init(&opowiadanie_rycerzy_cond[i],&attrcond);
	}
	
	pthread_attr_init(&thread);
	pthread_attr_setdetachstate(&thread,PTHREAD_CREATE_DETACHED);
	pthread_create(&kelner,&thread,kelner_thread,NULL);
	pthread_attr_setdetachstate(&thread,PTHREAD_CREATE_JOINABLE);
	pthread_create(&tids[0],&thread,king_thread,&ids[0]);
	
	for(i=1;i<=n;i++)
	{
		pthread_create(&tids[i],&thread,knight_thread,&ids[i]);
	}
	
	for(i = 0;i<=n;i++)
	{
		if(pthread_join(tids[i],NULL)<0)
			ERR("join");
	}
	
	free(opowiadanie_rycerzy_cond);
	free(opowiadanie_rycerzy);
	free(dostepnosc_dla_rycerza_cond);
	free(czy_czeka);
	free(tids);
	free(ids);
	free(naczynia);
	exit(EXIT_SUCCESS);
}
int hard_sleep(int seconds,int nanoseconds)
{
	struct timespec tt, t;
	t.tv_sec = seconds; 
	t.tv_nsec = nanoseconds;
	for(tt=t;nanosleep(&tt,&tt);)
		if(EINTR!=errno) ERR("nanosleep:");
	return 0;
}
void oddaj_kielich_talerzyk(int kto)
{
	/*Po skonczonym posilku rycerz odklada naczynia*/
	int w ,o,sasiad;
	indeksy(kto,&w,&o);
	if(pthread_mutex_lock(&dost_dla_rycerza)<0)ERR("lock");
	if(o < 0) o = n;
	naczynia[w] = 1;
	naczynia[o] = 1;
	/*Jesli ktorys z sasiadow oczekuje na zwolnienie sie naczyn
	 *  i po odlozeniu przez obecnego rycerza oba naczynia beda
	 *  dostepne jest to sygnalizowane danemu sasiadowi*/
	if((sasiad = kto - 1) < 0) sasiad = n;
	if(czy_czeka[sasiad] && czy_ma_naczynia(sasiad))
		if(pthread_cond_signal(&dostepnosc_dla_rycerza_cond[sasiad])<0)ERR("signal");
		
	if((sasiad = kto + 1) > n) sasiad = 0;
	if(czy_czeka[sasiad] && czy_ma_naczynia(sasiad))
		if(pthread_cond_signal(&dostepnosc_dla_rycerza_cond[sasiad])<0)ERR("signal");
	if(pthread_mutex_unlock(&dost_dla_rycerza)<0)ERR("unlock");
	
}
/*Funkcja zwraca indeks talerzyka i kielicha dla danego biesiadnika 
 * --ostatecznie jest to zbedne, gdyz biesiadnicy podnosza i oddaja 
 * oba naczynia na raz.
 * */
void indeksy(int kto,int* kielich,int* talerz)
{

	int w,o;
	if(kto == 0)
	{
		w = 0;
		o = n;
	}
	else if(kto % 2)
	{
		o = kto;
		w = kto - 1;
	}
	else
	{
		w = kto;
		o = kto - 1;
	}
	*kielich = w;
	*talerz = o;
}
/*
 * Funkcja sprawdza czy biesiadnik o indeksie kto,
 * moze podniesc oba naczynia
 * */
int czy_ma_naczynia(int kto)
{
	if(kto == 0)
		return naczynia[n] && naczynia[0];
	return naczynia[n-1] && naczynia[n];
}

void zabierz_kielich_talerzyk(int kto)
{
	int w,o,cnt = 1 ;
	indeksy(kto,&w,&o);
	if(pthread_mutex_lock(&dost_dla_rycerza)<0)ERR("lock");
	do
	{
		/*biesiadnik nie zabiera naczyn gdy nie sa oba dostepne, lub sasiaduje z krolem i krol rowniez czeka na naczynia*/
		if( naczynia[w] != 1 || naczynia[o] != 1 || (czy_czeka[0] && (kto == 1 || kto == n)) )
		{
			czy_czeka[kto] = 1;
			fprintf(stdout,"biesiadnik %d czeka na naczynia (%d kolejke)\n",kto,cnt++);
			if(pthread_cond_wait(&dostepnosc_dla_rycerza_cond[kto],&dost_dla_rycerza)<0)ERR("wait");
		}
		else
			break;
	}while(1);
	
	naczynia[w] = 0;
	naczynia[o] = 0;
	czy_czeka[kto] = 0;
	if(pthread_mutex_unlock(&dost_dla_rycerza)<0)ERR("unlock");

}
int zabierz_wino_ogorka(int kto)
{
	int cnt = 0,krol_czeka = 0;
	if(pthread_mutex_lock(&dobra_lock)<0)ERR("lock");
	do
	{
		if(pthread_mutex_lock(&dost_dla_rycerza)<0)ERR("lock");
		krol_czeka = czy_czeka[0];
		if(pthread_mutex_unlock(&dost_dla_rycerza)<0)ERR("unlock");
		/* jesli w trakcie oczekiwania na wino/ogorka krol rowniez chce spozywac,
		 *  rycerz oddaje mu naczynia*/
		if((kto == 1 || kto == n) && krol_czeka)
		{
			if(pthread_mutex_unlock(&dobra_lock)<0)ERR("unlock"); /* rycerz w tym momencie nie bedzie bral juz zywnosci*/
			if(pthread_mutex_lock(&dost_dla_rycerza)<0)ERR("lock");
			if(kto == 1)
			{
				naczynia[0] = 1;
				naczynia[1] = 1;
			}
			else
			{
				naczynia[n] = 1;
				naczynia[n-1] = 1;
			}
			if(pthread_mutex_unlock(&dost_dla_rycerza)<0)ERR("lock");
			if(czy_ma_naczynia(0))
				if(pthread_cond_signal(&dostepnosc_dla_rycerza_cond[0])<0)ERR("signal");
			fprintf(stdout,"Rycerz %d oddal naczynie krolowi\n",kto);
			return -1;
		}
		/*Jesli wino, lub ogorki sa niedostepne biesiadnik czeka na kelnera*/
		if(dzban < 1 || ogorki < 1)
		{
			fprintf(stdout,"biesiadnik %d czeka dostawe (minely go %d) \n",kto,cnt++);
			if(pthread_cond_wait(&dobra_cond,&dobra_lock)<0)ERR("wait");

		}
		else
			break;
	}while(1);
	
	/*Zabierane sa zasoby*/
	dzban--;
	ogorki--;
	
	fprintf(stdout,"biesiadnik %d zabral wino i ogorka  --  pozostalo %d wina oraz %d ogorkow\n",kto,dzban,ogorki);
	if(pthread_mutex_unlock(&dobra_lock)<0)ERR("unlock");
	return 0;
	
}
void opowiadanie(int kto, biesiadnik_t typ)
{
	int sec = rand() % (MAXRANGE - MINRANGE) + MINRANGE;
	char tytul[20];
	if(typ == RYCERZ)
		strcpy(tytul,"Rycerz");
	else
		strcpy(tytul,"Krol");
	/*Biesiadnik zaczyna opowiadac. opowiadanie_rycerzy[kto] zostalo
	 *  ustawione na 1 bezposrednio po zakonczeniu oczekiwania na swoja kolej*/
	fprintf(stdout,"%s %d bedzie opowiadac przez %d ms\n",tytul,kto,sec);
	hard_sleep(0,sec*1000000);
	
	if(pthread_mutex_lock(&opowiadanie_rycerzy_mutex)<0)ERR("lock");
	fprintf(stdout,"%s %d konczy opowiadanie po %d ms\n",tytul,kto,sec);
	opowiadanie_rycerzy[kto] = 0;
	if(pthread_mutex_unlock(&opowiadanie_rycerzy_mutex)<0)ERR("lock");
	/*Po zakonczeniu opowiesci informowani sa o tym wszyscy oczekujacy rycerze*/
	if(pthread_cond_broadcast(&opowiadanie_rycerzy_cond[kto])<0)ERR("broadcast");
			
}
void spozywanie(int kto, biesiadnik_t typ)
{
	int spozyto = 0;
	while(!spozyto)
	{
		zabierz_kielich_talerzyk(kto);
		/*w przypadku gdy rycerz spozyl posilek funkcja zwraca 0,
		 *w p.p. zwraca -1(gdy rycerz odstepuje naczynia krolowi)
		 * */
		spozyto = zabierz_wino_ogorka(kto) == 0; 
	}
	oddaj_kielich_talerzyk(kto);
}
void czekaj_na_dogodny_moment(int kto,biesiadnik_t typ,int na_opowiadanie)
{
	int sasiad_lewy,sasiad_prawy;
	char tytul[20];
	if(typ == RYCERZ)
		strcpy(tytul,"Rycerz");
	else
		strcpy(tytul,"Krol");
	if( (sasiad_lewy = kto - 1) < 0 )  sasiad_lewy  = n;
	if( (sasiad_prawy = kto + 1) > n ) sasiad_prawy =  0;
	
	pthread_mutex_lock(&opowiadanie_rycerzy_mutex);
	do
	{
		if(opowiadanie_rycerzy[sasiad_lewy])
		{
			fprintf(stdout,"%s %d czeka, az sasiad %d skonczy opowiadac\n",tytul,kto,sasiad_lewy);	
			if(pthread_cond_wait(&opowiadanie_rycerzy_cond[sasiad_lewy],&opowiadanie_rycerzy_mutex)<0)ERR("wait");

		}
		else if(opowiadanie_rycerzy[sasiad_prawy])
		{	
			fprintf(stdout,"%s %d czeka, az sasiad %d skonczy opowiadac\n",tytul,kto,sasiad_prawy);	
			if(pthread_cond_wait(&opowiadanie_rycerzy_cond[sasiad_prawy],&opowiadanie_rycerzy_mutex)<0)ERR("wait");
		}
		/*jesli oczekujacym jest Rycerz, sprawdza on dodatkowo,
		 *  czy nie przemawia Krol*/
		else if(typ == RYCERZ && opowiadanie_rycerzy[0])
		{
			fprintf(stdout,"Rycerz %d czeka, az  Krol skonczy opowiadac\n",kto);
			if(pthread_cond_wait(&opowiadanie_rycerzy_cond[0],&opowiadanie_rycerzy_mutex)<0)ERR("wait");
		}
		else 
			break;
	}while(1);
	/*Jesli biesiadnik oczekiwal az sasiad lub krol skonczy opowiadac,
	 *  aby samemu zaczac mowic, zastrzega sobie prawo do glosu*/
	if(na_opowiadanie)
	{
		fprintf(stdout,"%s %d uzyskal prawo do glosu\n",tytul,kto);
		opowiadanie_rycerzy[kto] = 1;
	}
	if(pthread_mutex_unlock(&opowiadanie_rycerzy_mutex)<0)ERR("unlock");
}

void* king_thread(void* arg)
{
	int i = 0;
	int id = *((int*)arg);
	for(;i<MAX_SHOTS;i++)
	{
		czekaj_na_dogodny_moment(0,KROL,1);
		opowiadanie(0,KROL);
		czekaj_na_dogodny_moment(0,KROL,0);
		spozywanie(id,KROL);	
		fprintf(stdout,"Krol skonczyl %d kolejke\n",i);

	}
	fprintf(stdout,"Krol skonczyl biesiadowac\n");
	return NULL;
}

void* knight_thread(void* arg)
{
	int id = *((int*)arg),i;
	for(;i<MAX_SHOTS;i++)
	{
		czekaj_na_dogodny_moment(id,RYCERZ,1);
		opowiadanie(id, RYCERZ);
		czekaj_na_dogodny_moment(id,RYCERZ,0);
		spozywanie(id, RYCERZ);
		fprintf(stdout,"Rycerz %d skonczyl %d kolejke\n",id,i);
	}
	fprintf(stdout,"Rycerz %d skonczyl biesiadowac\n",id);
	return NULL;
}

void* kelner_thread(void* arg)
{
	int limit  = (n+1)*10,uzupelnienia = 0;
	while(uzupelnienia < limit)
	{
		if(hard_sleep(0,rand()%300+200)<0)
			ERR("kelner-sleep");
		
		if(pthread_mutex_lock(&dobra_lock)<0)
			ERR("kelner-lock");
		
		if(dzban < n / 2 && ogorki < n / 3)
		{
			uzupelnienia++;
		
			dzban = n / 2;
			ogorki = n / 3;
			fprintf(stdout,"uzupelniono dzban  o i dostarczono ogorki.  -- dzban ma %d kielichow wina, dostepnych ogorkow %d\n", dzban, ogorki);
			if(pthread_cond_broadcast(&dobra_cond)<0)ERR("broadcast");
		}
		
		
		if(pthread_mutex_unlock(&dobra_lock)<0)
			ERR("kelner-unlock");

	}
	fprintf(stdout,"Kelner skonczyl prace\n");
	return NULL;
}

void usage(char* name)
{
	fprintf(stdout,"%s n\n",name);
	fprintf(stdout,"n in [5 , ...]\n");
	exit(EXIT_FAILURE);
}
